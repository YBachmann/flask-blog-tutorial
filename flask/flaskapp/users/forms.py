from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, BooleanField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo, Length, ValidationError
from flask_login import current_user
from flaskapp.db_models import User


class RegistrationForm(FlaskForm):
    username = StringField("Username", validators=[
                           DataRequired(), Length(min=2, max=20)])
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    confirm_password = PasswordField(
        "Password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Sign Up")

    def validate_username(self, username_form):
        # check if a user with this username already exists in the db
        user = User.query.filter_by(username=username_form.data).first()
        if user is not None:
            raise ValidationError(
                "Username already is taken. Please choose a different one. ")

    def validate_email(self, email_form):
        # check if a user with this email already exists in the db
        user = User.query.filter_by(email=email_form.data).first()
        if user is not None:
            raise ValidationError(
                "Email already is taken. Please choose a different one. ")


class LoginForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember = BooleanField("Remember Me")
    submit = SubmitField("Log In")


class UpdateAccountForm(FlaskForm):
    username = StringField("Username", validators=[
                           DataRequired(), Length(min=2, max=20)])
    email = StringField("Email", validators=[DataRequired(), Email()])
    picture = FileField("Update Profile Picture", validators=[
                        FileAllowed(["jpg", "png"])])
    submit = SubmitField("Update")

    def validate_username(self, username_form):
        if username_form.data != current_user.username:
            # check if a user with this username already exists in the db
            user = User.query.filter_by(username=username_form.data).first()
            if user is not None:
                raise ValidationError(
                    "Username already is taken. Please choose a different one. ")

    def validate_email(self, email_form):
        if email_form.data != current_user.email:
            # check if a user with this email already exists in the db
            user = User.query.filter_by(email=email_form.data).first()
            if user is not None:
                raise ValidationError(
                    "Email already is taken. Please choose a different one. ")


class RequestResetForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    submit = SubmitField("Request Password Reset")

    def validate_email(self, email_form):
        # check if a user with this email already exists in the db
        user = User.query.filter_by(email=email_form.data).first()
        if user is None:
            raise ValidationError(
                "There is no account with that email. Please register an account. ")


class ResetPasswordForm(FlaskForm):
    password = PasswordField("Password", validators=[DataRequired()])
    confirm_password = PasswordField(
        "Password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Reset Password")
