import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flask_mail import Message
from flaskapp import mail


def save_picture(form_picture):
    # generate random image file name
    random_hex = secrets.token_hex(8)
    _, file_extension = os.path.splitext(form_picture.filename)
    picture_filename = random_hex + file_extension
    picture_path = os.path.join(
        current_app.root_path, "static/profile_pics", picture_filename)
    # resize image
    output_size = (125, 125)
    resized_image = Image.open(form_picture)
    resized_image.thumbnail(output_size)
    # save image
    resized_image.save(picture_path)
    return picture_filename


def send_reset_email(user):
    token = user.get_reset_token()
    link = url_for("users.reset_token", token=token, _external=True)
    with current_app.app_context():
        msg = Message("uTime Password Reset Request",
                      sender="support.utime@gmail.com", recipients=[user.email])
        msg.body = """To reset your password, visit the following link:

{}

If you did not make this request then simply ignore this email and no changes will be made. 
""".format(link)
        mail.send(msg)
