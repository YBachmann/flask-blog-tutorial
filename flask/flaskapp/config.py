import os


class Config:
    SECRET_KEY = os.environ.get("UTIME_SECRET_KEY")

    SQLALCHEMY_DATABASE_URI = os.environ.get(
        "UTIME_DB_URI", "sqlite:///site.db")

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587  # use 465 for SSL
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = os.environ.get("UTIME_EMAIL_USER")
    MAIL_PASSWORD = os.environ.get("UTIME_EMAIL_PASS")
